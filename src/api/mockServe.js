import requests from "@/utils/mockAjax";

export const reqMockData = () => requests({ url: '/data', method: 'GET' });