// 将所有的都引入 并去别名
import * as trademark from '@/api/product/trademark'
import * as sku from '@/api/product/sku'
import * as spu from '@/api/product/spu'
import * as attr from '@/api/product/attr'

// 默认暴露
export default {
    trademark,
    sku,
    spu,
    attr
}