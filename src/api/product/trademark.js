import request from '@/utils/request'

//  GET /admin/product/baseTrademark/{page}/{limit}  获取品牌管理列表
export const reqTradeMorkList = (page, limit) => request({ url: `/admin/product/baseTrademark/${page}/${limit}`, method: 'GET' });

// DELETE /admin/product/baseTrademark/remove/{id}
export const reqRemoveTradeMork = (id) => request({ url: `/admin/product/baseTrademark/remove/${id}`, method: 'DELETE' });

// POST /admin/product/baseTrademark/save 添加品牌 要两个参数 品牌LOGO 品牌名称

// PUT /admin/product/baseTrademark/update 修改品牌 要三个参数 id 品牌LOGO 品牌名称
export const reqAddOrUpdateTradeMork = (tradeMark) => {
    // id 存在是修改 不存在是新增 将两个接口何为一个函数
    if (tradeMark.id) {
        return request({
            url: `/admin/product/baseTrademark/update`, method: 'PUT',
            data: tradeMark
        })
    } else {
        return request({
            url: `/admin/product/baseTrademark/save`, method: 'POST',
            data: tradeMark
        });
    }
}