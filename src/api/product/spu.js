import request from '@/utils/request'

// GET /admin/product/{page}/{limit}  page 当前页 limit 每一页的数据
export const reqSpuList = (page, limit, Category3Id) => request({ url: `/admin/product/${page}/${limit}`, params: { Category3Id }, method: 'GET' });
/**
 * axios中
 * get请求中的params配置项会拼接在地址上 参数query的形式
 * { url: `/admin/product/${page}/${limit}`, params: { Category3Id }, methods: 'GET' }
 * /admin/product/1/2?Category3Id=3
 * 
 * get请求中如果data配置项 不会生效
 * { url: `/admin/product/${page}/${limit}`, data: { Category3Id }, methods: 'GET' }
 * /admin/product/1/2
 * 
 * post请求中 使用params配置项 也是拼接在地址上
 * { url: `/admin/product/${page}/${limit}`, params: { Category3Id }, method: 'post' });
 * /admin/product/1/2?Category3Id=3
 * 
 * post请求中 使用data配置项 也是拼接在地址上
 * { url: `/admin/product/${page}/${limit}`, data: { Category3Id }, method: 'post' });
 * data的参数在请求体
 * 
 */

// GET /admin/product/getSpuById/{spuId} 获取spu列表 修改的回显
export const reqSpuInfoById = (spuId) => request({ url: `/admin/product/getSpuById/${spuId}`, method: 'GET' });

// GET /admin/product/baseTrademark/getTrademarkList spu中的下拉
export const reqTradeMarkList = () => request({ url: `/admin/product/baseTrademark/getTrademarkList`, method: 'GET' });

// GET /admin/product/spuImageList/{spuId}  获取spu的照片
export const reqSpuImgList = (spuId) => request({ url: `/admin/product/spuImageList/${spuId}`, method: 'GET' });

// GET /admin/product/baseSaleAttrList 获取平台的所有的销售属性
export const reqbaseSaleAttrList = () => request({ url: `/admin/product/baseSaleAttrList`, method: 'GET' });

// 保存和修改的请求基本一致，一个带id一个不带id
// POST /admin/product/saveSpuInfo
// POST /admin/product/updateSpuInfo
export const reqUpdataOrSaveSpuInfo = (spuInfo) => {
  if (spuInfo.id) {
    return request({ url: `/admin/product/updateSpuInfo`, method: 'POST', data: spuInfo })
  } else {
    return request({ url: `/admin/product/saveSpuInfo`, method: 'POST', data: spuInfo })
  }
};

// POST /admin/product/saveSpuInfo 保存spuInfo
export const reqSaveSpuInfo = () => request({ url: `/admin/product/saveSpuInfo`, method: 'POST' });

// GET /admin/product/spuSaleAttrList/{spuId} 获取销售属性的数据
export const reqSpuSaleAttrList = (spuId) => request({ url: `/admin/product/spuSaleAttrList/${spuId}`, method: 'GET' });

// GET /admin/product/attrInfoList/{category1Id}/{category2Id}/{category3Id}
export const reqAttrInfoList = (category1Id, category2Id, category3Id) => request({ url: `/admin/product/attrInfoList/${category1Id}/${category2Id}/${category3Id}`, method: 'GET' });

// POST /admin/product/saveSkuInfo 保存sku的修改
export const reqSaveSkuInfo = (skuInfo) => request({ url: `/admin/product/saveSkuInfo`, method: 'POST', data: skuInfo });

// DELETE /admin/product/deleteSpu/{spuId} 删除spu
export const reqDeleteSpu = (spuId) => request({ url: `/admin/product/deleteSpu/${spuId}`, method: 'DELETE' });

// GET /admin/product/findBySpuId/{spuId} 获取sku列表
export const reqSkuList = (spuId) => request({ url: `/admin/product/findBySpuId/${spuId}`, method: 'GET' });

// GET /admin/product/findBySpuId/{spuId}
export const reqFindSup = (spuId) => request({ url: `/admin/product/findBySpuId/${spuId}`, method: 'GET' });