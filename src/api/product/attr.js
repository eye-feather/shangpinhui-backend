import request from '@/utils/request'

// GET /admin/product/getCategory1 获取一级分类
export const reqCategory1List = () => request({ url: "/admin/product/getCategory1", method: 'GET' })

// GET /admin/product/getCategory2/{category1Id} 根据一级分类 获取二级分类
export const reqCategory2List = (category1Id) => request({ url: `/admin/product/getCategory2/${category1Id}`, method: 'GET' })

// GET /admin/product/getCategory3/{category2Id} 根据二级分类 获取三级分类
export const reqCategory3List = (category2Id) => request({ url: `/admin/product/getCategory3/${category2Id}`, method: 'GET' })

// GET /admin/product/attrInfoList/{category1Id}/{category2Id}/{category3Id}  获取attr属性列表
export const reqAttrList = (category1Id, category2Id, category3Id) => request({ url: `/admin/product/attrInfoList/${category1Id}/${category2Id}/${category3Id}`, method: 'GET' })

// DELETE /admin/product/removeCategory3/{id} 删除三级分类
export const reqDeleteAttr = (id) => request({ url: `/admin/product/removeCategory3/${id}`, method: 'DELETE' })


export const reqAddOrUpdataAttr = (data) => request({ url: `/admin/product/saveAttrInfo`, data, method: 'POST' })
/**
{
    "attrName": "string",// 属性名
    "attrValueList": [  // 属性信息
        {
            "attrId": 0,
            "valueName": "string" // 属性值
        }
    ],
    "categoryId": 0, // 分类id category3Id
    "categoryLevel": 3, // 
}
 */