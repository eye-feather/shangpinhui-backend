import request from '@/utils/request'

// GET /admin/product/list/{page}/{limit}  sku中获取数据表格
export const reqSkuTableList = (page,limit)=> request({url: `/admin/product/list/${page}/${limit}`,method: 'GET'});

// GET /admin/product/cancelSale/{skuId} 下架
export const reqCancelSale = (skuId)=> request({url: `/admin/product/cancelSale/${skuId}`,method: 'GET'});

// GET /admin/product/onSale/{skuId}  上架
export const reqOnSale = (skuId)=> request({url: `/admin/product/onSale/${skuId}`,method: 'GET'});

// GET /admin/product/getSkuById/{skuId} 获取sku详情
export const reqSkuInfo = (skuId)=> request({url: `/admin/product/getSkuById/${skuId}`,method: 'GET'});