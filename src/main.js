import Vue from 'vue'

import 'normalize.css/normalize.css' // A modern alternative to CSS resets

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import locale from 'element-ui/lib/locale/lang/zh-CN' // lang i18n

import '@/styles/index.scss' // global css

import App from './App'

import store from './store'
import router from './router'


// 引入API 所有的接口
import API from '@/api/index'

import HintButton from '@/components/HintButton'

import '@/icons' // icon
import '@/permission' // permission control

import '@/mock/mock'

/**
 * If you don't want to use mock-server
 * you want to use MockJs for mock api
 * you can execute: mockXHR()
 *
 * Currently MockJs will be used in the production environment,
 * please remove it before going online ! ! !
 */
if (process.env.NODE_ENV === 'production') {
  const { mockXHR } = require('../mock')
  mockXHR()
}

// set ElementUI lang to EN
Vue.use(ElementUI, { locale })
// 如果想要中文版 element-ui，按如下方式声明
// Vue.use(ElementUI)
// 注册自己封装的element-button组件
Vue.component(HintButton.name,HintButton);

Vue.config.productionTip = false

// 使用$bus类似的方式将API绑定到Vue原型上
Vue.prototype.$API = API;

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
