import { login, logout, getInfo } from '@/api/user'
import { getToken, setToken, removeToken } from '@/utils/auth'
import { resetRouter, asyncRoutes, anyRoutes, constantRoutes } from '@/router'// 异步路由

const getDefaultState = () => {
  return {
    token: getToken(),
    name: '',
    avatar: '',
    routes: [],// 后端返回的可以访问的routes
    buttons: [],//按钮权限
    roles: '',// 用户角色信息 管理员 或者 用户
    resultAsyncRoutes: [],//整理之后的异步路由
    resultRoutes: [],//最终结果的路由
  }
}

const state = getDefaultState()

const mutations = {
  RESET_STATE: (state) => {
    Object.assign(state, getDefaultState())
  },
  SET_TOKEN: (state, token) => {
    state.token = token
  },
  // SET_NAME: (state, name) => {
  //   state.name = name
  // },
  // SET_AVATAR: (state, avatar) => {
  //   state.avatar = avatar
  // },
  // 存储用户信息 和权限
  SET_USERINFO(state, userInfo) {
    state.name = userInfo.name;
    state.avatar = userInfo.avatar;
    state.routes = userInfo.routes;
    state.buttons = userInfo.buttons;
  },
  // 存储结果路由
  SET_RESULTANYROUTE(state, asyncRoutes) {
    // 存储整理好的异步路由
    state.resultAsyncRoutes = asyncRoutes;
    // 常量路由 concat 数组合并方法 合并最终的异步路由 和 其他路由
    state.resultRoutes = constantRoutes.concat(state.resultAsyncRoutes, anyRoutes);
  }
}

const computedAsyncRoutes = (asyncRoutes, routes) => {
  // console.log(asyncRoutes);
  // console.log(routes);
  return asyncRoutes.filter(item => {
    if (routes.indexOf(item.name) != -1) {
      if (item.children && item.children.length) {
        item.children = computedAsyncRoutes(item.children, routes);
      }
      return true;
    }
  })
}

const actions = {
  // user login
  async login({ commit }, userInfo) {
    const { username, password } = userInfo;
    let result = await login({ username: username.trim(), password: password });
    // console.log(result);
    if (result.code == 20000) {
      commit('SET_TOKEN', result.data.token)
      setToken(result.data.token)
      return 'ok';
    } else {
      return Promise.reject(new Error('faile'));
    }
  },

  // get user info
  getInfo({ commit, state }) {
    return new Promise((resolve, reject) => {
      getInfo(state.token).then(response => {
        const { data } = response
        // console.log(data);

        if (!data) {
          return reject('Verification failed, please Login again.')
        }

        const { name, avatar } = data

        // commit('SET_NAME', name)
        // commit('SET_AVATAR', avatar)
        commit('SET_USERINFO', data);
        commit('SET_RESULTANYROUTE', computedAsyncRoutes(asyncRoutes, data.routes));// 整理路由并保存
        resolve(data)
      }).catch(error => {
        reject(error)
      })
    })
  },

  // user logout
  logout({ commit, state }) {
    return new Promise((resolve, reject) => {
      logout(state.token).then(() => {
        removeToken() // must remove  token  first
        resetRouter()
        commit('RESET_STATE')
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },

  // remove token
  resetToken({ commit }) {
    return new Promise(resolve => {
      removeToken() // must remove  token  first
      commit('RESET_STATE')
      resolve()
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

