import Vue from 'vue'
import Vuex from 'vuex'
import getters from './getters'
import app from './modules/app'
import settings from './modules/settings'
import user from './modules/user'
import {reqMockData} from '@/api/mockServe'

Vue.use(Vuex)

const store = new Vuex.Store({
  actions:{
    // 获取mock的数据
    async getMockData(context){
      let result = await reqMockData();
      // console.log(result);
      if(result.code == 200){
        context.state.mockData = result.data;
      }
    }
  },
  state:{
    mockData:{}
  },
  modules: {
    app,
    settings,
    user
  },
  getters
})

export default store
